﻿using fr_stringcalculator.Source;
using NUnit.Framework;
using System;

namespace fr_stringcalculator.Tests
{
    [TestFixture]
    public class AddTests
    {
        [TestCase("", 0)]
        [TestCase("1", 1)]
        [TestCase("1,2", 3)]
        [TestCase("1,2,3,4,5,6,7,8,9,10", 55)]
        [TestCase("1\n2, 3", 6)]
        [TestCase("//;\n1;2", 3)]
        public void IfEntryNumbers_ReturnSum(string entry, int expected)
        {
            Calculator calculator = new Calculator();
            var result = calculator.Add(entry);
            Assert.That(result, Is.EqualTo(expected));
        }

        [TestCase("1,-2","-2")]
        [TestCase("1,-2,-3,4", "-2,-3")]
        [TestCase("//;\n1;-2;-3;4", "-2;-3")]
        public void IfEntryNumbersNegatif_ReturnThrow(string entry,string wrongNumbers)
        {
            Calculator calculator = new Calculator();
            Assert.That(() => calculator.Add(entry),
                              Throws.TypeOf<ArgumentException>()
                                    .With.Message.EqualTo($"negatives not allowed {wrongNumbers}"));
        }
    }
}
