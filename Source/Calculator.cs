﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace fr_stringcalculator.Source
{
    public class Calculator
    {
        private char delimmiter = ',';

        public int Add(string value)
        {
            if (ValueIsEmpty(value))
                return 0;

            if (IsDefinedNewDelimiter(value))
            {
                SetNewDelimiter(value);
                value = RemoveDelimiterDefinition(value);
            }

            value = ReplaceNewLinesWithDelimiters(value);

            IfNegativeNumbersGenerateArgumentException(value);

            return SumList(value);
        }

        private void IfNegativeNumbersGenerateArgumentException(string value)
        {
            var listNegativeNumbers = GetListValue(value).Where(x => ToInt(x) < 0).ToArray();

            if (listNegativeNumbers.Length > 0)
            {
                var negativeNumbers = string.Join(delimmiter.ToString(), listNegativeNumbers);
                throw new ArgumentException($"negatives not allowed {negativeNumbers}");
            }
        }

        private string RemoveDelimiterDefinition(string value)
        {
            var endFirstLine = 4;
            return value.Substring(endFirstLine);
        }

        private void SetNewDelimiter(string value)
        {
            var positionDelimiter = 2;
            delimmiter = value[positionDelimiter];
        }

        private bool IsDefinedNewDelimiter(string value)
        {
            return value.Length > 1 && value.Substring(0, 2).Equals("//");
        }

        private int SumList(string value)
        {
            int sum = 0;

            GetListValue(value).ForEach(number => sum += ToInt(number));

            return sum;
        }

        private List<string> GetListValue(string value)
        {
            return value.Split(delimmiter).ToList();
        }

        private string ReplaceNewLinesWithDelimiters(string value)
        {
            return value.Replace('\n', delimmiter);
        }

        private bool ValueIsEmpty(string value)
        {
            return value.Equals(string.Empty);
        }

        private int ToInt(string value)
        {
            return int.Parse(value);
        }
    }
}
